<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lead;

/* @var $this yii\web\View */
/* @var $model app\models\Deal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <!-- <?//= $form->field($model, 'leadId')->textInput() ?> -->
	
	
	<?php if($model->isNewRecord){ //show owner field only if isNewRecord activate => if we don't try to update lead, only creating a new one ?>
	<?= $form->field($model, 'leadId')->
				dropDownList(Lead::getLeads()) ?>
	<?php } //end of isNewRecord activate ?>
	
	

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
